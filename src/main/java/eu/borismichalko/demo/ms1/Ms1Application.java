package eu.borismichalko.demo.ms1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;

@SpringBootApplication
public class Ms1Application {

	@Autowired
	private CassandraTemplate cassandraTemplate;

	public static void main(String[] args) {
		SpringApplication.run(Ms1Application.class, args);
	}

	@Bean
	public CommandLineRunner createKeyspaceAndTable(CassandraOperations cassandraOperations) {
		return args -> {
			cassandraTemplate.getCqlOperations().execute(
					"CREATE KEYSPACE IF NOT EXISTS demo WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1'};");
			cassandraTemplate.getCqlOperations()
					.execute("CREATE TABLE IF NOT EXISTS demo.random_data (id UUID PRIMARY KEY, value double);");
		};
	}
}
