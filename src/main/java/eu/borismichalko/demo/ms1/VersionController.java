package eu.borismichalko.demo.ms1;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {
  @GetMapping("/api/v1/version")
  public ResponseEntity<String> getVersion() {
    return ResponseEntity.ok("Version 1.0");
  }
}
