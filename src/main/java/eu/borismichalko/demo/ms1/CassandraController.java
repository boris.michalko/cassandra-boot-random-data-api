package eu.borismichalko.demo.ms1;

import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CassandraController {

    @Autowired
        private CassandraTemplate cassandraTemplate;

    @PostMapping("/insert-random-data")
    public String insertRandomData() {
        Random random = new Random();
        UUID id = UUID.randomUUID();
        double value = random.nextDouble();
        cassandraTemplate.getCqlOperations().execute("INSERT INTO demo.random_data (id, value) VALUES (?, ?);", id, value);

        return String.format("Inserted random data: id=%s, value=%.2f%n", id, value);

    }
}
